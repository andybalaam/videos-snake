data class Point(var x: Int, var y: Int)

fun main() {
    val p = Point( x = 10, y = 10 )
    p.x = 3
    println("Immutability2")
}
