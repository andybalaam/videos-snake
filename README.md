# Slides for Snake First Impressions videos

Slides for the YouTube series
"[First Impressions (formed by writing snake)](https://www.youtube.com/watch?v=LZzr9wvlWaE&list=PLgyU3jNA6VjTYOJZVTYuxMz2UD7hi4Xsu)"
in which I write Snake in different languages and technologies, and use the
experience to form first impressions of them.

The code that goes with these slides is at [gitlab.com/andybalaam/snake](https://gitlab.com/andybalaam/snake).

If you want to look view the slides as a presentation, look at:

* [Snake in KotlinJS](http://www.artificialworlds.net/presentations/snake-kotlinjs/snake-kotlinjs.html)
* [Snake in Elm](http://www.artificialworlds.net/presentations/snake-elm/snake-elm.html)
* [Snake in ZX Spectrum Sinclair BASIC](http://www.artificialworlds.net/presentations/snake-zx-spectrum-basic/snake-zx-spectrum-basic.html)
* [Snake in Python 3 + Qt5](http://www.artificialworlds.net/presentations/snake-python3-qt5/snake-python3-qt5.html)
* [Snake in Ruby](http://www.artificialworlds.net/presentations/snake-ruby/snake-ruby.html)
* [Snake in Dart](http://www.artificialworlds.net/presentations/snake-dart/snake-dart.html)
* [Snake in Groovy](http://www.artificialworlds.net/presentations/snake-groovy/snake-groovy.html)


## License

Copyright Andy Balaam 2014-2019.  Released under CC-BY-SA. See
[LICENSE.md](LICENSE.md) for more info.
